(function($){

	$.fn.scrollTo = function(options){

		var options = $.extend({
			speed: 400
		}, options);

		var links = this;


		var make = function(){
			$(this).click(function( e ){
				$(links).removeClass('is-active');
				$(this).addClass('is-active');
				e.preventDefault();
				var section = $(this).attr('href');
				var top = $(section).offset().top;
				$('body, html').animate({
					scrollTop: top
				}, options.speed)
			})
		};

		return this.each(make)
	}

}(jQuery))
