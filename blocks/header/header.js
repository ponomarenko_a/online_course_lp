;(function(){

  $(window).scroll( function(){
    if( $(this).scrollTop() > 50 ){
      $('.header').addClass('header--scrolled')
    }else{
      $('.header').removeClass('header--scrolled')
    }
  } );

  $(".hamburger").click(function(){
    $(this).toggleClass("is-active");
    $('.main_nav').appendTo('header').slideToggle()
  });


}());
