(function(){

  var $programForm = $("#programForm");
  var $orderForm = $('#orderForm');

  $("input[name=phone]").mask("+380 (99) 999 - 99 - 99");

  $programForm.validate({
    rules: {
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      email: {
        required: 'Укажите ваш E-Mail',
        email: 'Это не правильный email',
      }
    },
    submitHandler: function(){
      var data = $programForm.serialize();
      $("#programMailLoad").addClass('mail-load--shown');
      $.ajax({
        type: 'POST',
        data: data,
        url: 'mail.php',
        success: function ( res ) {
          console.log( res );
          // dataLayer.push({ event: "zayavka"});
          $programForm.find('input').val('');
          $("#programMailLoad").addClass('mail-load--sended');
          setTimeout( function(){
              $("#programMailLoad").removeClass('mail-load--shown')
          }, 1000 );
            $.ajax({
                type: 'POST',
                data: data,
                url: 'mail2.php',
                success: function ( res ) {
                    console.log( res );
                }
            })
        }
      });
    }
  });

  $orderForm.validate({
    rules: {
      name: {
        required: true
      },
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      name: {
        required: 'Укажите ваше имя'
      },
      email: {
        required: 'Укажите ваш E-Mail',
        email: 'Это не правильный email',
      }
    },
    submitHandler: function(){
      var data = $orderForm.serialize();
        $("#orderMailLoad").addClass('mail-load--shown');
        $.ajax({
            type: 'POST',
            data: data,
            url: 'mail3.php',
            success: function ( res ) {
                console.log( res );
                // dataLayer.push({ event: "zayavka"});
                $orderForm.find('input').val('');
                $("#orderMailLoad").addClass('mail-load--sended');
                setTimeout( function(){
                    $("#orderMailLoad").removeClass('mail-load--shown')
                }, 1000 );
                $.ajax({
                    type: 'POST',
                    data: data,
                    url: 'mail4.php',
                    success: function ( res ) {
                        console.log( res );
                    }
                })
            }
        });
    }
  });

}());

(function($){

	$.fn.scrollTo = function(options){

		var options = $.extend({
			speed: 400
		}, options);

		var links = this;


		var make = function(){
			$(this).click(function( e ){
				$(links).removeClass('is-active');
				$(this).addClass('is-active');
				e.preventDefault();
				var section = $(this).attr('href');
				var top = $(section).offset().top;
				$('body, html').animate({
					scrollTop: top
				}, options.speed)
			})
		};

		return this.each(make)
	}

}(jQuery))

;(function(){

  $(window).scroll( function(){
    if( $(this).scrollTop() > 50 ){
      $('.header').addClass('header--scrolled')
    }else{
      $('.header').removeClass('header--scrolled')
    }
  } );

  $(".hamburger").click(function(){
    $(this).toggleClass("is-active");
    $('.main_nav').appendTo('header').slideToggle()
  });


}());
