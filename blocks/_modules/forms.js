(function(){

  var $programForm = $("#programForm");
  var $orderForm = $('#orderForm');

  $("input[name=phone]").mask("+380 (99) 999 - 99 - 99");

  $programForm.validate({
    rules: {
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      email: {
        required: 'Укажите ваш E-Mail',
        email: 'Это не правильный email',
      }
    },
    submitHandler: function(){
      var data = $programForm.serialize();
      $("#programMailLoad").addClass('mail-load--shown');
      $.ajax({
        type: 'POST',
        data: data,
        url: 'mail.php',
        success: function ( res ) {
          console.log( res );
          // dataLayer.push({ event: "zayavka"});
          $programForm.find('input').val('');
          $("#programMailLoad").addClass('mail-load--sended');
          setTimeout( function(){
              $("#programMailLoad").removeClass('mail-load--shown')
          }, 1000 );
            $.ajax({
                type: 'POST',
                data: data,
                url: 'mail2.php',
                success: function ( res ) {
                    console.log( res );
                }
            })
        }
      });
    }
  });

  $orderForm.validate({
    rules: {
      name: {
        required: true
      },
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      name: {
        required: 'Укажите ваше имя'
      },
      email: {
        required: 'Укажите ваш E-Mail',
        email: 'Это не правильный email',
      }
    },
    submitHandler: function(){
      var data = $orderForm.serialize();
        $("#orderMailLoad").addClass('mail-load--shown');
        $.ajax({
            type: 'POST',
            data: data,
            url: 'mail3.php',
            success: function ( res ) {
                console.log( res );
                // dataLayer.push({ event: "zayavka"});
                $orderForm.find('input').val('');
                $("#orderMailLoad").addClass('mail-load--sended');
                setTimeout( function(){
                    $("#orderMailLoad").removeClass('mail-load--shown')
                }, 1000 );
                $.ajax({
                    type: 'POST',
                    data: data,
                    url: 'mail4.php',
                    success: function ( res ) {
                        console.log( res );
                    }
                })
            }
        });
    }
  });

}());
