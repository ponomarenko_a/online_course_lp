<?php

require 'phpLibs/PHPMailer/PHPMailerAutoload.php';

$mailClient = new PHPMailer;

$orderName = trim($_POST["name"]);
$orderEmail = trim($_POST["email"]);
$orderCourse = trim($_POST["course"]);
$duration = 18;
$price = 5100;

if( $orderCourse == "start" ){
    $duration = 7;
    $price = 2100;
}else if( $orderCourse == "pro" ){
    $duration = 11;
    $price = 3300;
}

//$mailAdmin->SMTPDebug = 3;                               // Enable verbose debug output
$mailClient->CharSet = 'UTF-8';
$mailClient->isSMTP();                                      // Set mailAdminer to use SMTP
$mailClient->Host = 'pod51015.outlook.com';  // Specify main and backup SMTP servers
$mailClient->SMTPAuth = true;                               // Enable SMTP authentication
$mailClient->Username = 'frontend@cbsystematics.com';                 // SMTP username
$mailClient->Password = 'Qsrtg458!5$3qwDT';                           // SMTP password
// $mailAdmin->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mailClient->Port = 587;                                    // TCP port to connect to

$mailClient->setFrom('frontend@cbsystematics.com', 'Front-end');
// $mailAdmin->addAddress('alexandr.ponomarenko@icloud.com');    // Name is optional
$mailClient->addAddress($orderEmail);
$mailClient->addReplyTo('info@space-academy.com.ua', 'Information');
// $mailAdmin->addCC('alexandr.ponomarenko@icloud.com');
// $mailAdmin->addBCC('alexandr.ponomarenko@icloud.com');

//$mailClient->addAttachment('course.pdf');
$mailClient->AddEmbeddedImage('img/space_academy.png', 'sa_logo');
$mailClient->AddEmbeddedImage('img/itvdn_black.png', 'itvdn_logo');
$mailClient->AddEmbeddedImage('img/letter-bg.jpg', 'letter_bg');
//$mailClient->AddEmbeddedImage('img/icons/002-vk.png', 'vk');
//$mailClient->AddEmbeddedImage('img/icons/004-fb.png', 'fb');
//$mailClient->AddEmbeddedImage('img/icons/003-inst.png', 'inst');
//$mailClient->AddEmbeddedImage('img/icons/001-youtube.png', 'youtube');
$mailClient->isHTML(true); // Set emailAdmin format to HTML

$mailClient->Subject = 'Спасибо за заявку на курс Front-End ONLINE';

$mailClient->Body    = '
<html lang="en" style="font-family: "Open-Sans", sans-serif;">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Raleway:400,700&amp;amp;subset=cyrillic" rel="stylesheet"/>
</head>
<body style="width: 600px; margin: 0 auto;line-height:1.4;">
<table style="width: 100%;">
    <tr>
        <td style="padding: 20px 0 20px 15px; box-sizing: border-box;"><img src="cid:sa_logo" style="height: 30px; margin-right: 20px;"/><img src="cid:itvdn_logo" style="height: 40px;"/></td>
        <td style="padding: 20px 15px 20px 0; box-sizing: border-box;"><a href="mailto:info@space-academy.com.ua" style="color: #000; font-size:12px; text-decoration: none; margin-right: 20px;">info@space-academy.com.ua</a><a href="tel:0999033738" style="color: #000; font-size:12px; text-decoration: none;">+38(099)903-37-38</a></td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 100px; font-size: 30px; text-transform: uppercase; font-weight: 700; color: #fff; background-image: url(cid:letter_bg);">Курс Front-end developer ONLINE</td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 50px;">
            <h3 style="margin: 0;font-size: 20px; font-weight: 700; color: #5833FB;">Здравствуйте, '.$orderName.'!</h3>
            <p>Мы получили Вашу заявку на курс <a href="http://frontend.itvdn.com/" style="font-weight: 700; text-decoration: underline;">Front-End Developer Online</a>. Наш менеджер свяжется с Вами в ближайшее время для уточнения деталей и ответа на все ваши вопросы касательно обученияю</p>
            <div style="color: #fff; background-color: #5833FB; padding: 40px;">
                <h3 style="margin: 0 0 20px;font-size: 18px; font-weight: 700; color: #fff; text-transform: uppercase;">информация о курсе:</h3>
                <p style="margin-bottom: 7px;">Тренер - <span style="font-weight: 700;">Александр Пономаренко</span></p>
                <p style="margin-bottom: 7px;">Дата начала курса - <span style="font-weight: 700;">04.08.2017</span></p>
                <p style="margin-bottom: 7px;">Продолжительность курса - <span style="font-weight: 700;">'.$duration.' недель</span></p>
                <p>Стоимость - <span style="font-weight: 700;">'.$price.' грн</span></p>
            </div>
            <div style="margin-top: 50px;">
                С уважением,<br>команда Space Academy<br>
                Телефон: <a href="tel:0968843502">+38(096)884-35-02</a><br>
                E-Mail: <a href="mailto:info@space-academy.com.ua">info@space-academy.com.ua</a>
            </div>
        </td>
    </tr>
</table>
</body>
</html>
';

// $mailClient->AltBody = 'Ваша программа тут: https://drive.google.com/open?id=0B3rrAgSMMsiyb0syUHNJTVBMM28';

if(!$mailClient->send()) {
echo 'Message for client could not be sent.';
echo 'Mailer Error: ' . $mailClient->ErrorInfo;
} else {
echo 'Message for client has been sent';
};